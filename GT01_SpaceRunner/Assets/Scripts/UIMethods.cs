﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIMethods : MonoBehaviour
{
    [SerializeField]
    private GameObject _tutorial;
    [SerializeField]
    private GameObject _closeMenu;

    public void ChangeToGameScene()
    {
        SceneManager.LoadScene("01_GameScene");
        Time.timeScale = 1;
    }
    public void OpenTutorial()
    {
        Debug.Log("OPEN");
        _tutorial.SetActive(true);
        _closeMenu.SetActive(false);
    }

    public void ChangeToMainMenu()
    {
        SceneManager.LoadScene("00_MainMenu");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
