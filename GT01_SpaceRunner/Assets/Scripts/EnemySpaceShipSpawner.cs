﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpaceShipSpawner : MonoBehaviour
{
    [SerializeField]
    private float _startTime = 60f; //Spawn spaceship after one minute of playtime

    [SerializeField]
    private GameObject _spaceship;

    private float _spawnBounds = 2.5f;

    [SerializeField]
    private float _repeatingTimeMin = 60;
    [SerializeField]
    private float _repeatingTimeMax = 90;

    private SpaceshipPowerup _spaceshipPowerup;

    private void Awake()
    {
        _spaceshipPowerup = _spaceship.GetComponent<SpaceshipPowerup>();
    }
    void Start()
    {
        Invoke("SpawnSpaceship", _startTime);
    }

    private void SpawnSpaceship()
    {
        if (_spaceshipPowerup._IsBlackHoleEnabled) return;
        Instantiate(_spaceship, new Vector3(Random.Range(-_spawnBounds, _spawnBounds), 3, 1), Quaternion.Euler(0, 0, -180));
        Invoke("SpawnSpaceship", Random.Range(_repeatingTimeMin, _repeatingTimeMax) / GameManager._difficultyMultiplier);
        //Spawn Another space ship between 60 and 90 seconds, This time is reduced as the difficulty is increased

    }
}
