﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreMultiplierBarUpdater : MonoBehaviour
{
    private static Image _image;

    [SerializeField]
    private float _smoothFactor = 5f;

    private static float _newVal;

    public static void UpdateBar(float newValue)
    {
        _image.fillAmount = Mathf.InverseLerp(0f, GameManager._maxMultiplierLimit, newValue);
    }


    private void Awake()
    {
        _newVal = 0f;
        _image = GetComponent<Image>();
    }

    // private void Update()
    // {
    //     if (_newVal > _image.fillAmount)  //Smoothly lerp slider bar forward
    //     {
    //         _image.fillAmount = Mathf.Lerp(_image.fillAmount, _newVal, _smoothFactor * Time.fixedDeltaTime);
    //     }
    //     else      //Smoothly lerp slider bar forward
    //     {
    //         _image.fillAmount = Mathf.Lerp(_newVal, _image.fillAmount, _smoothFactor * Time.fixedDeltaTime);
    //     }
    // }
}
