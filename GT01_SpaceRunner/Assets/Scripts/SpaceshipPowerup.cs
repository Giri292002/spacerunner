﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipPowerup : MonoBehaviour
{
    public bool hasPowerup = false; //Only get powerup if no powerup is already enabled

    [SerializeField]
    private GameObject _invincibilityPowerup;
    [SerializeField]
    public float _invincibilityTime = 5f; //Changed by Health function also to enable invinciblity for 1 second when hit an object

    [SerializeField]
    private float _tripleShotTime = 5f;

    [SerializeField]
    private GameObject _BFGPowerup;
    [SerializeField]
    private float _BFGTime = 6f;

    [SerializeField]
    private float _blackholeTime = 5f;

    [SerializeField]
    private GameObject _blackholeFX;
    private float _ogMult = 0; //multiplier value before blackhole powerup was applied
    public bool _IsBlackHoleEnabled = false;
    private float _blackholeRevupTime = 2f;
    private bool _resetBlackHole = true;
    private float _lerpValue = 0f;



    ///////////////////////////////////INVINCIBILITY/////////////////////////////////////////////
    public void EnableInvincibility()
    {
        GetComponent<Health>()._invincible = true;
        Invoke("DisableInvincibilty", _invincibilityTime);
        _invincibilityPowerup.gameObject.SetActive(true);
        _invincibilityTime = 5f;
        hasPowerup = true;
    }
    private void DisableInvincibilty()
    {
        GetComponent<Health>()._invincible = false;
        _invincibilityPowerup.gameObject.SetActive(false);
        hasPowerup = false;
    }
    ///////////////////////////////////TRIPLE SHOT/////////////////////////////////////////////
    public void EnableTripleShot()
    {
        GetComponent<SpaceshipShooter>()._currentShootType = SpaceshipShooter.ShootTypes.Triple;
        Invoke("DisableTripleShot", _tripleShotTime);
        hasPowerup = true;
    }
    private void DisableTripleShot()
    {
        GetComponent<SpaceshipShooter>()._currentShootType = SpaceshipShooter.ShootTypes.Basic;
        hasPowerup = false;
    }

    ///////////////////////////////////BFG/////////////////////////////////////////////

    public void EnableBFG()
    {
        _BFGPowerup.gameObject.SetActive(true);
        GetComponent<SpaceshipShooter>().ShootBFG = true;
        Invoke("DisableBFG", _BFGTime);
        hasPowerup = true;
    }
    private void DisableBFG()
    {
        _BFGPowerup.gameObject.GetComponent<BFGBullet>().DisableBFG();
        GetComponent<SpaceshipShooter>().ShootBFG = false;
        hasPowerup = false;
    }

    ///////////////////////////////////HEALTH/////////////////////////////////////////////

    public void IncreaseHealth()
    {
        GetComponent<Health>().IncreaseHealth();
    }

    // ///////////////////////////////////BLACKHOLE - DISABLED BUGGY/////////////////////////////////////////////
    // public void EnableBlackhole()  //increase difficulty to 50 which will make the scene super fast and then revert it back to original difficulty
    // {
    //     _ogMult = GameManager._difficultyMultiplier;
    //     _IsBlackHoleEnabled = true;
    //     _blackholeFX.SetActive(true);
    //     _invincibilityTime = _blackholeTime + 2f;
    //     EnableInvincibility();
    //     hasPowerup = true;
    //     Invoke("DisableBlackhole", _blackholeTime);
    // }

    // private void DisableBlackhole()
    // {

    //     _blackholeFX.SetActive(false);
    //     GameManager._difficultyMultiplier = _ogMult;

    // }

}
