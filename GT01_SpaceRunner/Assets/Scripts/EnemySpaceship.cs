﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnemySpaceship : MonoBehaviour
{
    [SerializeField]
    private Vector3 _finalPos;

    private Vector3 _startPos;

    [SerializeField]
    private bool _isRelative = true;

    [SerializeField]
    private float _duration = 1f;
    [SerializeField]
    private float _attackTime = 5f;

    private Animator _BFGanimator;

    [SerializeField]
    private GameObject _BFGBullet;

    private void Start()
    {
        _BFGanimator = _BFGBullet.GetComponent<Animator>();
        _startPos = transform.position;
        Tweener moveTween = transform.DOMove(_finalPos, _duration);
        moveTween.SetRelative(_isRelative);
        moveTween.SetEase(Ease.Linear);
        Invoke("StartEnemyAttack", _duration); //Start attack after move animation is compelete
        Invoke("StopEnemyAttack", _attackTime * GameManager._difficultyMultiplier); //Stop the enemy spaceship attack after some time.
    }

    private void StartEnemyAttack()
    {
        _BFGBullet.SetActive(true);
    }
    private void StopEnemyAttack()
    {
        _BFGanimator.SetTrigger("StopBFG");
        Tweener moveTween = transform.DOMove(_startPos, _duration);
        Invoke("DestroyMe", _duration);//Destroy the spaceship after it has finished moving out of the frame;
    }
    private void DestroyMe()
    {
        Destroy(gameObject);
    }





    [ExecuteInEditMode]
    private void OnDrawGizmosSelected()
    {
        Vector3 dest = _finalPos;
        if (_isRelative)
        {
            dest = transform.position + _finalPos;
        }
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, dest);
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(dest, 0.2f);
    }

}
