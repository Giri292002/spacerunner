﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetSpawner : MonoBehaviour
{
    private float _spawnBounds = 2.5f;

    [SerializeField]
    private GameObject _planetPrefab;

    private void Start()
    {
        SpawnPlanet();
    }

    private void SpawnPlanet()
    {
        Instantiate(_planetPrefab, new Vector3(Random.Range(-_spawnBounds, _spawnBounds), 3, 1), transform.rotation);
        Invoke("SpawnPlanet", Random.Range(10f, 20f));
    }

    private void Update()
    {

    }
}
