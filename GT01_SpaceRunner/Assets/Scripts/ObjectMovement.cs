﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMovement : MonoBehaviour
{

    [SerializeField]
    private float _meteorSpeed = 4f;

    private void Start()
    {
        Invoke("AddBoundsDestroy", 1.5f);
    }

    void Update()
    {
        float _movementMultiplier = _meteorSpeed * GameManager._difficultyMultiplier * Time.deltaTime;
        transform.position = new Vector3(transform.position.x, (transform.position.y - _movementMultiplier), transform.position.z);
    }

    private void AddBoundsDestroy()
    {
        gameObject.AddComponent<BoundsDestroy>();
    }
}
