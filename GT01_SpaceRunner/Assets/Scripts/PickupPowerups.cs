﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CapsuleCollider2D))]
public class PickupPowerups : MonoBehaviour
{
    [SerializeField]
    private powerups _powerups;

    [SerializeField]
    private GameObject _sfxPickupPowerup;

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.GetComponent<SpaceshipPowerup>() != null)
        {
            Instantiate(_sfxPickupPowerup, transform.position, transform.rotation);
            if (other.GetComponent<SpaceshipPowerup>().hasPowerup == false) // List of powerups that can work alone
            {
                switch (_powerups)
                {
                    case powerups.invincibility:
                        other.GetComponent<SpaceshipPowerup>().EnableInvincibility();
                        Destroy(gameObject);
                        break;

                    case powerups.tripleshot:
                        other.GetComponent<SpaceshipPowerup>().EnableTripleShot();
                        Destroy(gameObject);
                        break;
                    case powerups.BFG:
                        other.GetComponent<SpaceshipPowerup>().EnableBFG();
                        Destroy(gameObject);
                        break;
                    case powerups.heart:
                        if (other.GetComponent<Health>()._currentHealth < other.GetComponent<Health>()._maxhealth) //Only ppickup heart powerup when you took damage
                        {
                            other.GetComponent<SpaceshipPowerup>().IncreaseHealth();
                            Destroy(gameObject);
                        }
                        break;
                        // case powerups.blackhole:
                        //     other.GetComponent<SpaceshipPowerup>().EnableBlackhole();
                        //     break;

                }
            }
            else // List of powerups that can work even other powerups are enabled
            {
                switch (_powerups)
                {
                    case powerups.heart:
                        if (other.GetComponent<Health>()._currentHealth < other.GetComponent<Health>()._maxhealth) //Only ppickup heart powerup when you took damage
                        {
                            other.GetComponent<SpaceshipPowerup>().IncreaseHealth();
                            Destroy(gameObject);
                        }
                        break;
                }
            }
        }
    }



    enum powerups
    {
        invincibility,
        tripleshot,
        BFG,
        heart,
        blackhole
    }
}
