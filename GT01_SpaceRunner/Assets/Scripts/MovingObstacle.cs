﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MovingObstacle : MonoBehaviour
{
    [SerializeField]
    private float _offsetMin = 5;
    [SerializeField]
    private float _offsetMax = 10;

    [SerializeField]
    private float _movingDuration = 0.5f;

    private void Start()
    {
        _movingDuration *= GameManager._difficultyMultiplier;
        Vector3 _offset;
        _offset = new Vector3(Random.Range(_offsetMin, _offsetMax), 0, 0);
        Tweener moveTween = transform.DOMove(_offset, _movingDuration * GameManager._difficultyMultiplier);
        moveTween.SetRelative(true);
        moveTween.SetLoops(-1, LoopType.Yoyo);
        moveTween.SetEase(Ease.Linear);
    }
}
