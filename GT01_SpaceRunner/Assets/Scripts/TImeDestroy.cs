﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TImeDestroy : MonoBehaviour
{
    [SerializeField]
    private float _timeToDestroy;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroyThis", _timeToDestroy);
    }

    private void DestroyThis()
    {
        Destroy(gameObject);
    }
}
