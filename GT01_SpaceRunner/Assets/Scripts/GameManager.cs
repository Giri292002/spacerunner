﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    static public int _score = 0;
    static public float _difficultyMultiplier = 1;

    private float _scoreTime = 0f;
    private float _scoreUpdateTime = 0.1f;
    private int _aliveScore = 1; //points you earn for every _scoreUpdateTime youve survived.

    public static float _currentMultiplier = 0;
    private float _multiplierDelta = 0.5f;
    private float _decreaseMultiplierDelta = 0.25f;
    public static int _maxMultiplierLimit = 2; //The maximum limit before the multiplier reset
    public static int _scoreMultiplier = 1; //x2
    private float _multiplierTime = 0f;
    private float _multiplierUpdateTime = 5f;

    [SerializeField]
    private GameObject _multiplerText;


    private void Start()
    {
        _score = 0;
        _difficultyMultiplier = 1;
        _maxMultiplierLimit = 2;
        _scoreMultiplier = 1;
        _currentMultiplier = 0;
    }

    private void Update()
    {
        multiplyDifficulty();
        increaseAliveScore();
        decreaseMultiplierTimer(); //Reduce ScoreMultiplier overtime
        _multiplierUpdateTime = 5f / _scoreMultiplier; //Decrease multiplier decrease time as multiplier increases

    }

    private void increaseAliveScore()
    {
        _scoreTime += Time.deltaTime;
        if (_scoreTime >= (_scoreUpdateTime / _scoreMultiplier))
        {
            _score += _aliveScore;
            _scoreTime -= _scoreTime;
        }
    }
    private void decreaseMultiplierTimer()
    {
        _multiplierTime += Time.deltaTime;
        if (_multiplierTime >= _multiplierUpdateTime)
        {
            decreaseMultiplier();
            _multiplierTime -= _multiplierTime;
        }
    }

    void multiplyDifficulty()  //Increase game difficulty as time passes
    {
        _difficultyMultiplier += 0.02f * Time.deltaTime;
    }

    public void increaseMultiplier()
    {
        _currentMultiplier += _multiplierDelta;
        ScoreMultiplierBarUpdater.UpdateBar(_currentMultiplier);
        if (_currentMultiplier >= _maxMultiplierLimit)
        {
            _currentMultiplier = 0.2f;
            ScoreMultiplierBarUpdater.UpdateBar(_currentMultiplier);
            _scoreMultiplier++;
            _multiplerText.GetComponent<ScoreMultiplierTextUpdate>().CallChangeColor();
            _multiplierDelta = _multiplierDelta / 2;
        }
    }

    public void decreaseMultiplier()
    {
        if (_currentMultiplier > 0)
        {
            _currentMultiplier -= _decreaseMultiplierDelta;
            ScoreMultiplierBarUpdater.UpdateBar(_currentMultiplier);
            if (_currentMultiplier <= 0)
            {
                if (_scoreMultiplier - 1 < 1)   //if x2 decreases and multiplier is less than 1 set it to 1
                {
                    _scoreMultiplier = 1;
                    ScoreMultiplierBarUpdater.UpdateBar(_currentMultiplier);
                }
                else if (_scoreMultiplier - 1 == 1)
                {
                    _scoreMultiplier = 1;
                    _currentMultiplier = _maxMultiplierLimit;
                    ScoreMultiplierBarUpdater.UpdateBar(_currentMultiplier);
                    _multiplierDelta = _multiplierDelta * 2;

                }
                else
                {
                    _scoreMultiplier--;
                    _currentMultiplier = _maxMultiplierLimit;
                    ScoreMultiplierBarUpdater.UpdateBar(_currentMultiplier);
                    _multiplierDelta = _multiplierDelta * 2;
                }

            }
        }
    }

}
