﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class ResourceRandomizer : MonoBehaviour
{

    private SpriteRenderer _sprite;
    private Destructables _destructableComp;

    [SerializeField]
    private bool _tutorial;
    private void Start()
    {
        _sprite = GetComponent<SpriteRenderer>();
        _destructableComp = GetComponent<Destructables>();
        _sprite.color = Color.blue;
        ChangeColor();
        if (_tutorial)
        {
            Invoke("ChangeColor", 1);
        }
    }

    private void ChangeColor()
    {
        if (Random.value > 0.5) //50% chance
        {
            _sprite.color = new Color(183, 123, 18, 255);
            if (!_tutorial)
                _destructableComp._score = 50;                //Spawn medium resource asteroid with color brown
        }
        if (Random.value > 0.2) //80% chance
        {
            _sprite.color = Color.white;
            if (!_tutorial)
                _destructableComp._score = 10;                //Spawn common resource asteroid with color white
        }
        if (Random.value > 0.8) // 20% chance
        {
            _sprite.color = new Color(154, 0, 205, 255);
            if (!_tutorial)
                _destructableComp._score = 100;                //Spawn rare resource asteroid with color purple
        }
        if (_tutorial)
        {
            Invoke("ChangeColor", 1);
        }

    }
}
