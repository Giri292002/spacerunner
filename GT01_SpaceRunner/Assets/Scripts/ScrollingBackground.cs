﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour
{

    [SerializeField]
    private float _speed = -2f;

    [SerializeField]
    private float _yBounds = 5f;

    private Vector3 _startTransform;

    [SerializeField]
    private bool _isBlackhole;

    private void Start()
    {
        _startTransform = gameObject.transform.position;
    }

    void Update()
    {

        transform.Translate(0f, _speed * Time.deltaTime * GameManager._difficultyMultiplier, 0f);
        // if (_isBlackhole == false)
        // {
        // //     
        // // }
        // // else
        // // {
        // //     transform.Translate(0f, _speed * Time.deltaTime, 0f);
        // // }

        if (transform.position.y <= -_yBounds)
        {
            transform.position = new Vector3(0, _yBounds, 0);
        }
    }

    public void ResetBGPosition()
    {
        gameObject.transform.position = _startTransform;
    }
}
