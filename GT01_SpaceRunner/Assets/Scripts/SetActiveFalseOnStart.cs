﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetActiveFalseOnStart : MonoBehaviour
{
    private void Start()
    {
        gameObject.SetActive(false);
    }
}
