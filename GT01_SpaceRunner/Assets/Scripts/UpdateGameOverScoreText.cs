﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class UpdateGameOverScoreText : MonoBehaviour
{
    private TMP_Text _text;
    private void Awake()
    {
        _text = GetComponent<TMP_Text>();
    }

    private void OnEnable()
    {
        _text.text = $"{GameManager._score}";
    }
}
