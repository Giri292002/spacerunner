﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class PlanetRandomizer : MonoBehaviour
{

    [SerializeField]
    private Sprite[] _planets;

    private SpriteRenderer _spriteRenderer;

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        int _scale = Random.Range(1, 4);
        transform.localScale = new Vector3(_scale, _scale, _scale);
        _spriteRenderer.sprite = _planets[Random.Range(0, _planets.Length)];
    }
}
