using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructables : MonoBehaviour
{

    [SerializeField]
    public int _score;

    [SerializeField]
    private GameObject _explosionParticle;

    [SerializeField]
    private GameManager _gameManager;

    [SerializeField]
    private GameObject _explosionSFX;

    private void Awake()
    {
        _gameManager = FindObjectOfType<GameManager>();
    }

    public void DestroyMePlayer()
    {

        GameManager._score += _score;
        _gameManager.increaseMultiplier();
        Instantiate(_explosionSFX, transform.position, transform.rotation);
        Instantiate(_explosionParticle, transform.position, transform.rotation);
        Destroy(gameObject);
    }

    public void DestroyMeEnemy()
    {
        Instantiate(_explosionSFX, transform.position, transform.rotation);
        Instantiate(_explosionParticle, transform.position, transform.rotation);
        Destroy(gameObject);
    }

}
