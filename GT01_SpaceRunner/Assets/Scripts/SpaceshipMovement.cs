﻿using UnityEngine;

public class SpaceshipMovement : MonoBehaviour
{

    [SerializeField]
    private float _moveSpeed = 2.0f;

    [SerializeField]
    private float _boundsX = 2.0f;

    [SerializeField]
    private KeyCode _leftKey = KeyCode.LeftArrow;
    [SerializeField]
    private KeyCode _rightKey = KeyCode.RightArrow;

    private moveStates _currentState = moveStates.canMoveBoth;

    void Update()
    {
        Movement();
    }

    private void Movement() //Take input from a held down key and use it to move left and right accordingly taking movement bounds into consideration
    {
        CheckBounds();
        float _horizontalInput;
        if (Input.GetKey(_leftKey) && _currentState == moveStates.onlyLeft || Input.GetKey(_leftKey) && _currentState == moveStates.canMoveBoth)
        {
            _horizontalInput = -1;
        }
        else if (Input.GetKey(_rightKey) && _currentState == moveStates.onlyRight || Input.GetKey(_rightKey) && _currentState == moveStates.canMoveBoth)
        {
            _horizontalInput = 1;
        }
        else
        {
            _horizontalInput = 0;
        }

        float _movementValue = _moveSpeed * _horizontalInput * Time.deltaTime;
        transform.position = new Vector3(transform.position.x + _movementValue, transform.position.y, transform.position.z);


    }

    private void CheckBounds()  //Function to check the spaceship remains within screen space
    {
        if (transform.position.x >= _boundsX)
        {
            _currentState = moveStates.onlyLeft;
        }
        else
        if (transform.position.x <= -_boundsX)
        {
            _currentState = moveStates.onlyRight;
        }
        else
            _currentState = moveStates.canMoveBoth;

    }

    enum moveStates
    {
        onlyRight,
        onlyLeft,
        canMoveBoth
    }


}



