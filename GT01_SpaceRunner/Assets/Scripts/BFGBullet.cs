﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(BoxCollider2D))]
public class BFGBullet : MonoBehaviour
{
    private Animator _animator;
    private BoxCollider2D _collider;

    [SerializeField]
    private bool _isPlayer = false;

    [SerializeField]
    private GameObject _bfgSFX;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _collider = GetComponent<BoxCollider2D>();
        _collider.enabled = false;
    }
    private void OnEnable()
    {
        Instantiate(_bfgSFX, transform.position, transform.rotation);
        _animator.SetBool("FinishBFG", false);
        _collider.enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<Destructables>() != null)
        {
            if (_isPlayer == true)
            {
                other.gameObject.GetComponent<Destructables>().DestroyMePlayer();
            }
            else
            {
                other.gameObject.GetComponent<Destructables>().DestroyMeEnemy();
            }
        }
        else if (other.gameObject.GetComponent<SpaceshipMovement>())
        {

        }
        else
        {
            Destroy(other.gameObject);
        }
    }

    public void DisableBFG()
    {
        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        if (gameObject.GetComponent<SpaceshipPowerup>() != null)
        {
            gameObject.GetComponent<SpaceshipPowerup>().EnableInvincibility();
            _animator.SetBool("FinishBFG", true);
            _collider.enabled = false;
        }
    }
}
