﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundsDestroy : MonoBehaviour
{
    [SerializeField]
    private float _bounds = 3.5f;


    void Update()
    {

        if (transform.position.x >= _bounds || transform.position.x <= -_bounds || transform.position.y >= _bounds || transform.position.y <= -_bounds)
        {
            Destroy(gameObject);
        }



    }
}
