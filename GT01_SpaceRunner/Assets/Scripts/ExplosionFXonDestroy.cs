﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionFXonDestroy : MonoBehaviour
{
    [SerializeField]
    private GameObject _ExplosionFX;

    private SpriteRenderer _renderer;

    [SerializeField]
    private GameObject _explosionSFX;

    private void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Hazard"))
        {
            _renderer.enabled = false;
            Instantiate(_explosionSFX, transform.position, transform.rotation);
            Instantiate(_ExplosionFX, transform.position, transform.rotation);
            Invoke("DestroyMe", 1.3f);
        }
    }

    private void DestroyMe()
    {
        Destroy(gameObject);
    }
}
