﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipShooter : MonoBehaviour
{
    [SerializeField]
    private float _bulletSpawnOffset;
    [SerializeField]
    private float _startZRotation = 19;
    private float _zRotationOffset = -19;

    private Vector3 _spawnPos;

    private KeyCode _shootButton = KeyCode.Space;

    public ShootTypes _currentShootType = ShootTypes.Basic;
    public bool ShootBFG = false; //Doesnt shoot normal bullet when this variable is true

    [SerializeField]
    private GameObject _basicBullet;

    [SerializeField]
    private GameObject _shootSFX;

    private void Update()
    {

        if (Input.GetKeyDown(_shootButton) && ShootBFG == false)
        {
            Shoot();
        }
    }

    private void tripleShoot() //3 bullets that shoot in a cone shape
    {
        float originalZRotation = _startZRotation;
        for (int i = 0; i <= 2; i++)
        {
            Instantiate(_shootSFX, transform.position, transform.rotation);
            _spawnPos = transform.position + transform.up * _bulletSpawnOffset;
            Quaternion _rotation = Quaternion.Euler(0, 0, _startZRotation);
            SpawnBullet(_spawnPos, _rotation);
            _startZRotation += _zRotationOffset; //Increase rotation by zRotOffset to simulate cone type effect

        }
        _startZRotation = originalZRotation; //Reset spawn rotation after 3 bullets have been fired
    }

    private void Shoot() //Spawn bullet tpy ebased on current enum
    {
        Instantiate(_shootSFX, transform.position, transform.rotation);
        _spawnPos = transform.position + transform.up * _bulletSpawnOffset;
        switch (_currentShootType)
        {
            case ShootTypes.Basic:
                SpawnBullet(_spawnPos, transform.rotation);
                break;

            case ShootTypes.Triple:
                tripleShoot();
                break;

        }
    }


    private void SpawnBullet(Vector3 _bulletSpawnPosition, Quaternion _spawnRotation)
    {
        Instantiate(_basicBullet, _bulletSpawnPosition, _spawnRotation);
    }

    public enum ShootTypes
    {
        Basic,
        Triple,
        BFG
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position + transform.up * _bulletSpawnOffset, 0.08f);

    }
}