﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreMultiplierTextUpdate : MonoBehaviour
{

    private TMP_Text _text;
    private Color _ogColor;
    [SerializeField]
    private GameObject _sfxIncreaseMult;

    private void Awake()
    {
        _text = GetComponent<TMP_Text>();
        _ogColor = _text.color;
    }

    private void Update()
    {
        _text.text = $"Multipler: x{GameManager._scoreMultiplier}";
    }


    public void CallChangeColor()
    {
        StartCoroutine(ChangeColor());
    }

    private IEnumerator ChangeColor()
    {
        Instantiate(_sfxIncreaseMult, transform.position, transform.rotation);
        _text.color = Color.red;
        yield return new WaitForSeconds(0.25f);
        _text.color = Color.blue;
        yield return new WaitForSeconds(0.25f);
        _text.color = Color.green;
        yield return new WaitForSeconds(0.25f);
        _text.color = Color.cyan;
        yield return new WaitForSeconds(0.25f);
        _text.color = _ogColor;
    }

}
