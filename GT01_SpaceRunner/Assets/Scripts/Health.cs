﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    [SerializeField]
    public int _maxhealth = 5;

    public bool _invincible = false;

    public int _currentHealth;

    [SerializeField]
    private GameObject _gameOverScreen;

    [SerializeField]
    private int _numOfHearts = 3;

    [SerializeField]
    Image[] hearts;

    [SerializeField]
    private Sprite _fullHearts;

    [SerializeField]
    private Sprite _emptyHearts;

    private SpaceshipPowerup _powerup;
    private Animator _animator;

    [SerializeField]
    private GameObject _Camera;
    [SerializeField]
    private GameObject _SFXDamage;

    private void Awake()
    {
        _powerup = GetComponent<SpaceshipPowerup>();
        _animator = GetComponent<Animator>();
    }

    private void Start()
    {
        _currentHealth = _maxhealth;
    }

    void Update()
    {
        if (_currentHealth > _numOfHearts)
        {
            _currentHealth = _numOfHearts;
        }

        for (int i = 0; i < hearts.Length; i++)
        {
            if (i < _currentHealth)
            {
                hearts[i].sprite = _fullHearts;
            }
            else
            {
                hearts[i].sprite = _emptyHearts;
            }

            if (i < _numOfHearts)
            {
                hearts[i].enabled = true;

            }
            else
            {
                hearts[i].enabled = false;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Hazard") && _invincible == false)
        {
            Instantiate(_SFXDamage, transform.position, transform.rotation);
            _currentHealth--;
            _powerup._invincibilityTime = 1f;
            _powerup.EnableInvincibility();
            SpaceshipAnimationUpdate();
            CheckIfDead();

        }
    }

    private void SpaceshipAnimationUpdate()
    {
        _animator.SetTrigger("SpaceshipDamage");
        _Camera.GetComponent<CameraShake>().TriggerShake();

    }
    void CheckIfDead()
    {
        if (_currentHealth <= 0)
        {
            OnDeath();
        }
    }

    public void IncreaseHealth()
    {
        if (_currentHealth < _maxhealth)
        {
            _currentHealth++;
        }
    }

    private void OnDeath()
    {
        Time.timeScale = 0;
        Cursor.visible = true;
        _gameOverScreen.SetActive(true);
    }
}
