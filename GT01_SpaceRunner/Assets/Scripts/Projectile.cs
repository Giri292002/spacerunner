﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class Projectile : MonoBehaviour
{
    private Rigidbody2D _rigidBody;

    [SerializeField]
    private float _projectileSpeed = 3f;

    private BoxCollider2D _collision;

    private void Awake()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        _collision = GetComponent<BoxCollider2D>();
    }

    private void EnableCollision()
    {
        _collision.enabled = true;
    }

    private void Update()
    {
        MoveVel();
    }

    private void MoveVel()
    {
        _rigidBody.velocity = transform.up * _projectileSpeed;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {

        if (other.gameObject.GetComponent<Destructables>() != null)
        {
            other.gameObject.GetComponent<Destructables>().DestroyMePlayer();
        }
        if (other.gameObject.GetComponent<Projectile>() == null)
        {
            Destroy(gameObject);
        }
        else
        {
            _collision.enabled = false;
            Invoke("EnableCollision", 0.08f);
        }


    }
}
