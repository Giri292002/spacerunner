﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSpawners : MonoBehaviour
{
    [SerializeField]
    private GameObject _spawners;

    private int _spawnDelay = 10;

    private void Start()
    {

        SpawnAnotherSpawner();
    }

    private void SpawnAnotherSpawner()
    {
        Instantiate(_spawners, Vector3.zero, transform.rotation, this.transform);
        Invoke("SpawnAnotherSpawner", _spawnDelay); //Spawn a new spawner every _spawndelay seconds
        _spawnDelay += 20; //Increase delay in spawn so it spawning of spawner is not spammed
    }
}
