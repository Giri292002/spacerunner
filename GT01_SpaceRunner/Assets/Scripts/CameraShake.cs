﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    private Transform _transform;

    private float _shakeDuration = 0f;

    [SerializeField]
    private float _shakeMagnitude = 0.7f;

    private float _dampingSpeed = 1.0f;

    private Vector3 _initialPos;
    private void Awake()
    {
        if (transform == null)
        {
            _transform = GetComponent<Transform>();
        }
    }

    private void OnEnable()
    {
        _initialPos = transform.localPosition;
    }

    private void Update()
    {
        if (_shakeDuration > 0)
        {
            transform.localPosition = _initialPos + Random.insideUnitSphere * _shakeMagnitude;
            _shakeDuration -= Time.deltaTime * _dampingSpeed;
        }
        else
        {
            _shakeDuration = 0f;
            transform.localPosition = _initialPos;
        }
    }

    public void TriggerShake()
    {
        _shakeDuration = 0.5f;
    }
}
