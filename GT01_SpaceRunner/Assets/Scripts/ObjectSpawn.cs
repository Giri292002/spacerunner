﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawn : MonoBehaviour
{

    [SerializeField]
    private ObjectType _objectType = ObjectType.meteor;

    private float _spawnDelayMin;
    private float _spawnDelayMax;

    [SerializeField]
    private GameObject _meteorPrefab;

    [SerializeField]
    private GameObject[] _powerupPrefab;

    [SerializeField]
    private GameObject _resourcePrefab;

    private float _spawnBounds = 2.5f;

    private void Start()
    {
        Spawn();
    }

    private void Spawn()
    {
        switch (_objectType)
        {
            case ObjectType.meteor:

                Instantiate(_meteorPrefab, new Vector3(Random.Range(-_spawnBounds, _spawnBounds), 3, 1), transform.rotation);
                break;

            case ObjectType.resource:
                Instantiate(_resourcePrefab, new Vector3(Random.Range(-_spawnBounds, _spawnBounds), 3, 1), transform.rotation);
                break;
            case ObjectType.powerups:
                int _powerupToSpawn = ChooseAPowerup();
                Instantiate(_powerupPrefab[_powerupToSpawn], new Vector3(Random.Range(-_spawnBounds, _spawnBounds), 3, 1), transform.rotation);
                break;
        }

        ChangeObjectToSpawn();

        Invoke("Spawn", (Random.Range(1f, 2f) / GameManager._difficultyMultiplier));

    }

    private void ChangeObjectToSpawn()
    {
        _objectType = (ObjectType)Random.Range(0, 3);

    }

    private int ChooseAPowerup()
    {

        if (Random.value > 0.8)//Poweruponly happens when there is 20% 
        {
            if (Random.value > 0.4) //60% Chance of spawning stuff
            {
                return 1; // Spawn Invincibility
            }
            if (Random.value > 0.5) //50% Chance
            {
                return 4; //Spawn Heart powerup
            }
            if (Random.value > 0.6)// 40% chance of spawning
            {
                return 2; // Spawn Triple Shot
            }
            if (Random.value > 0.8) // 20% of spawning stuff
            {
                return 3; //Spawn BFG

            }

        }
        return 0;

    }

    enum ObjectType
    {
        meteor,
        powerups,
        resource
    }
}
