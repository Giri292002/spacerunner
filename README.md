##SPACE RUNNER v1.0.2##

####ABOUT####

- SPACE RUNNER is an infinite runner whose main goal is to get the highest score possible
- Avoid the meteors,asteroids on fire and enemy spaceships
- Shoot the resource asteroid to earn multiplier points that can boost your score.

####POWERUPS####
Many different power ups to help you reach your high score is available.

- Invincibility *Normal rarity*: Equip a shield that protects you from all incoming hazards.
- Triple Shot *Normal rarity*: Turn your single blaster into a three shot to damage more resources with 									a single shot
- Heart *Medium Rarity*: Earn back your heart that you lost when taking damage
- BFG *Super rare rarity*: An all powerful laser beam that obliterates anything and everything in your 									path

####CREDITS####
A game by P.B.Giridharahn

####ASSETS####

#####ART#####
- 42 Planets pack : [https://helianthus-games.itch.io/pixel-art-planets]
- Explosion Particle Pack : [https://ppeldo.itch.io/2d-pixel-art-game-spellmagic-fx]

#####SOUNDS#####
- Main Menu Theme: [https://www.youtube.com/watch?v=SM6NEoex4ws]
- In game Music: [https://www.youtube.com/watch?v=9fKadz4HlSk]
- Explosion: [https://www.youtube.com/watch?v=dyPIyjtB3eM]
- Laser shooting: [https://www.youtube.com/watch?v=b0INmd5xEdE]
- BFG shot: [https://www.youtube.com/watch?v=whLbGbpt-E4]
- Power up and Multiplier Increase: [https://www.youtube.com/watch?v=v94-AQxceGk&t=34s]

#####TUTORIALS#####
- Old school heart health: [https://www.youtube.com/watch?v=3uyolYVsiWc]